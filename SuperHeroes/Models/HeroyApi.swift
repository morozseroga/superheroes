//
//  HeroyApi.swift
//  SuperHeroes
//
//  Created by Сергей Мороз on 22.03.18.
//  Copyright © 2018 Сергей Мороз. All rights reserved.
//

import Foundation

struct HeroyApi: Decodable {

    let name: String
    let image: String
    let description: String
    let time: String
}
