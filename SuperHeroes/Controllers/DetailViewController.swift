//
//  DetailViewController.swift
//  SuperHeroes
//
//  Created by Сергей Мороз on 24.03.18.
//  Copyright © 2018 Сергей Мороз. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var heroy: HeroyApi!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        iconImageView.loadImageUsingCache(withUrl: heroy.image)
        nameLabel.text = heroy.name
        dateLabel.text = Date.getFormattedDate(string: heroy.time)
        descriptionLabel.text = heroy.description
    }
}
