//
//  MenuViewController.swift
//  SuperHeroes
//
//  Created by Сергей Мороз on 26.03.18.
//  Copyright © 2018 Сергей Мороз. All rights reserved.
//

import UIKit

class MenuViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice.current.userInterfaceIdiom == .phone {
            view.backgroundColor = .lightGray
        } else {
            view.backgroundColor = .white
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showMain" &&  UIDevice.current.userInterfaceIdiom == .phone {
            return false
        }
        return true
    }
}
