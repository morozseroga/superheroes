//
//  ListViewController.swift
//  SuperHeroes
//
//  Created by Сергей Мороз on 22.03.18.
//  Copyright © 2018 Сергей Мороз. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController, UISearchResultsUpdating {

    var pageViewController: PageViewController? = nil
    var objects = [HeroyApi]()
    var filteredContentList = [HeroyApi]()
    var menuView: UIView!
    var menuIsOpened = false
    
    var searchController = UISearchController()
    
    private let heigtMenu: CGFloat = 200.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let split = splitViewController {
            let controllers = split.viewControllers
            pageViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? PageViewController
        }
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            let menuButtom = UIBarButtonItem(title: "Menu", style: .done, target: self, action: #selector(showMenu))
            navigationItem.leftBarButtonItem = menuButtom
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC = storyboard.instantiateViewController(withIdentifier: String(describing: MenuViewController.self)) as! MenuViewController
            if let menuView = menuVC.view {
                self.menuView = menuView
                self.menuView.frame.size.width = heigtMenu
                self.menuView.frame.origin.x  = -heigtMenu
                addChildViewController(menuVC)
                menuView.backgroundColor = .lightGray
                view.addSubview(menuView)
            }
        }
        
        setFilterController()
        setRefreshControl()
        reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    private func setFilterController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
    }
    
    private func setRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl?.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }

    @objc private func reloadData() {
        DataMenager.shared.getHeroes { (result) in
            if let res = result {
                self.objects = res
            }
            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    @objc private func showMenu() {
        openMenu(open: menuIsOpened)
        menuIsOpened = !menuIsOpened
    }
    
    func openMenu(open: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.menuView.frame.origin.x = open ? -self.heigtMenu : 0
        }
    }
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let heroy = isFiltering() ? filteredContentList[indexPath.row] : objects[indexPath.row]
                let index = objects.index(where: { $0.name == heroy.name })
                let controller = segue.destination as! PageViewController
                controller.indexShow = (index != nil) ? index! : 0
                controller.heroes = objects
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering() ? filteredContentList.count : objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "heroyCell", for: indexPath) as! HeroyTableViewCell

        let object = isFiltering() ? filteredContentList[indexPath.row] : objects[indexPath.row]
        cell.setup(heroy: object)
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            objects.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchText = searchController.searchBar.text!
        filteredContentList = objects.filter { $0.name.contains(searchText) }
        tableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
}


