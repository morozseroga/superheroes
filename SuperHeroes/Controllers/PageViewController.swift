//
//  DetailViewController.swift
//  SuperHeroes
//
//  Created by Сергей Мороз on 22.03.18.
//  Copyright © 2018 Сергей Мороз. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    
    var heroes = [HeroyApi]()
    var indexShow: Int = 0
    var detailsViewControllers = [UIViewController]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        setupData()
        self.view.backgroundColor = .black
        self.turnToPage(index: indexShow)
    }
    
    private func setupData() {
        for heroy in heroes {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let detailVC = storyboard.instantiateViewController(withIdentifier: String(describing: DetailViewController.self)) as! DetailViewController
            detailVC.heroy = heroy
            detailsViewControllers.append(detailVC)
        }
    }
    
    private func turnToPage(index: Int) {
        let vc = detailsViewControllers[index]
        var direction = UIPageViewControllerNavigationDirection.forward
        if let currentVC = detailsViewControllers.first {
            if let currentIndex = detailsViewControllers.index(of: currentVC) {
                if currentIndex > index {
                    direction = .reverse
                }
            }
        }
        setViewControllers([vc], direction: direction, animated: true, completion: nil)
    }
}

extension PageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = detailsViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return detailsViewControllers.last
        }
        
        guard detailsViewControllers.count > previousIndex else {
            return nil
        }
        return detailsViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = detailsViewControllers.index(of: viewController) {
            if index < detailsViewControllers.count - 1 {
                return detailsViewControllers[index + 1]
            }
        }
        return detailsViewControllers.first
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return detailsViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return indexShow
    }
}
