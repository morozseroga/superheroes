//
//  HeroyTableViewCell.swift
//  SuperHeroes
//
//  Created by Сергей Мороз on 22.03.18.
//  Copyright © 2018 Сергей Мороз. All rights reserved.
//

import UIKit

class HeroyTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func setup(heroy: HeroyApi) {
        nameLabel.text = heroy.name
        dateLabel.text = Date.getFormattedDate(string: heroy.time)
        iconImageView.loadImageUsingCache(withUrl: heroy.image)
    }
}
