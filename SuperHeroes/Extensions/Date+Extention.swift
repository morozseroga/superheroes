//
//  Date+Extention.swift
//  SuperHeroes
//
//  Created by Сергей Мороз on 22.03.18.
//  Copyright © 2018 Сергей Мороз. All rights reserved.
//

import Foundation

extension Date {
    static func getFormattedDate(string: String) -> String {
        guard let dateInt = Double(string) else {
            return ""
        }
        let date = Date(timeIntervalSince1970: dateInt/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyy HH:mm"
        return dateFormatter.string(from: date)
    }
}
