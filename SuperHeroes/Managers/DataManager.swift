//
//  DataManager.swift
//  SuperHeroes
//
//  Created by Сергей Мороз on 22.03.18.
//  Copyright © 2018 Сергей Мороз. All rights reserved.
//

import Foundation

class DataMenager {
    
    static let shared = DataMenager()
    
    private let apiUrl = "http://test.php-cd.attractgroup.com/test.json"
    
    private init() {}
    
    func getHeroes(callback: @escaping (_ data: [HeroyApi]?)-> Void) {
        
        guard let url = URL(string: apiUrl) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                let gitData = try JSONDecoder().decode([HeroyApi].self, from: data)
                callback(gitData)
            } catch let err {
                print(err.localizedDescription)
            }
        }
        task.resume()
    }
}
